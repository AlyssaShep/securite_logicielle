(* Exercice 1 *)

Parameter A B C : Prop.
Goal A -> B -> A.
intros.
assumption.
Qed.

Goal (A -> B -> C) -> (A -> B) -> A -> C.
intros.
apply H.
assumption.
apply H0.
assumption.
Qed.

Goal A /\ B -> B.
intros.
apply H.
Qed.

Goal B -> A \/ B.
intros.
right.
assumption.
Qed.

Goal (A \/ B) -> (A -> C) -> (B -> C) -> C.
intros.
elim H.
assumption.
assumption.
Qed.

Goal A -> False -> ~A.
intros.
intro.
assumption.
Qed.

Goal False -> A.
intros.
elim H.
Qed.

Goal (A <-> B) -> A -> B.
intros.
apply H.
assumption.
Qed.

Goal (A <-> B) -> B -> A.
intros.
apply H.
assumption.
Qed.

Goal (A <-> B) -> (B -> A) -> (A <-> B).
intros.
assumption.
Qed.

(* Exercice 2 *)

Parameter E : Set.
Parameter Q P : E -> Prop.

Goal forall x : E, (P x) -> exists y : E, (P y) \/ (Q y).
intros.
exists x.
left.
assumption.
Qed.

Goal (exists x : E, (P x) \/ (Q x)) -> (exists y : E, (P y)) \/ (exists z : E, (Q z)).
intros.
elim H.
intros.
elim H0.
left.
exists x.
assumption.
right.
exists x.
assumption.
Qed.

Goal (forall x: E, P(x)) /\ (forall x: E, Q(x)) -> forall x: E, P(x) /\ Q(x).
intro.
elim H.
intros.
split.
apply H0.
apply H1.
Qed.

Goal (forall x : E, (P x) /\ (Q x)) -> (forall y : E, (P y)) /\ (forall z : E, (Q z)).
intros.
split.
apply H.
apply H.
Qed.

Goal (forall x : E, ~(P x)) -> ~(exists y : E, (P y)).
intros.
intro.
elim H0.
apply H.
Qed.

(*Goal ~(forall x : E, (P x)) -> exists y : E, ~(P y).
intro.*)

(* Exercice 4 *)
Parameter R : E -> E -> Prop.

Goal (exists x : E, forall y : E, (R x y)) -> (forall z : E, exists w : E, (R w z)).
intros.
elim H.
intros.
exists x.
apply H0.
Qed.

Goal (forall x : E, forall y : E, (R x y)) -> forall w : E, forall z : E, (R z w).
intros.
apply H.
Qed.

Goal (exists x : E, exists y : E, (R x y)) -> exists w : E, exists z : E, (R z w).
intros.
elim H.
intros.
elim H0.
intros.
exists x0.
exists x.
apply H1.
Qed.

Goal (exists x : E, forall y : E, (R x y)) -> forall w : E, exists z : E, (R z w).
intros.
elim H.
intros.
exists x.
apply H0.
Qed.

Goal forall x : E, (forall y : E, (P y) -> (P x)) -> (exists z : E, (P z)) -> (P x).
intros.
elim H0.
assumption.
Qed.

(*Variables a b : E.
Goal exists x : E, (P x) -> (P a) /\ (P b).
exists a.
intro.
split.
assumption. *)


