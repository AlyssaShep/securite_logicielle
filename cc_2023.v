Inductive bin : Set :=
|C0 : bin
|C1 : bin
|C0S : bin -> bin
|C1S : bin -> bin.

(* 
Inductive is_even : nat -> Prop :=
  | is_even_O : is_even O
  | is_even_S : forall n: nat, is_even n -> is_even(S (S n)).
(* q2 *)
Fixpoint even(n :nat) : bool :=
  match n with
  | O => true
  | 1 => false
  | (S (S n)) => even n
  end.
*)

Require Import FunInd.
Inductive is_even : bin -> Prop :=
| is_even_0 : is_even C0
| is_even_S : forall b : bin, is_even (C0S b).

(* 1) *)

Definition bin_6 := (C0S (C1S C1)).

Goal is_even (C0S (C1S C1)).
Proof.
apply is_even_S.
Qed.

(* 2) *)
Fixpoint head_0 (b : bin) : bin :=
match b with
| C1 => C1S C0
| C0 => C0S C0
| (C0S f) => (C0S (head_0 f))
| (C1S f) => (C1S (head_0 f))
end.

Goal head_0 bin_6 = (C0S (C1S (C1S C0))).
Proof.
simpl.
reflexivity.
Qed.

(* 3) *)

Inductive is_equal : bin -> bin -> Prop :=
| is_equal_0 : forall b : bin, is_equal (head_0 b) b
| is_equal_ref : forall b : bin, is_equal b b
| is_equal_sys : forall b c : bin, is_equal b c -> is_equal c b
| is_equal_trans : forall a b c : bin, is_equal a b -> is_equal b c -> is_equal a c.

Goal is_equal bin_6 (head_0 (head_0 bin_6)).
Proof.
apply is_equal_sys.
apply (is_equal_trans (head_0 (head_0 bin_6)) (head_0 bin_6) bin_6).
apply is_equal_0.
apply is_equal_0.
Qed.

(* 4) *)
Fixpoint mult_2 (b : bin) :=
match b with
| C0 => C0
| f => C0S f
end.

Goal is_equal (C0S bin_6) (mult_2 bin_6).
Proof.
simpl.
apply is_equal_ref.
Qed.

Lemma is_mult_k: forall b:bin, is_even b -> exists b':bin, is_equal b (mult_2 b').
Proof.
intros.
induction H.
exists C0.
apply is_equal_ref.
exists b.
unfold mult_2.
destruct b.
apply (is_equal_0 C0).
apply is_equal_ref.
apply is_equal_ref.
apply is_equal_ref.
Qed.




