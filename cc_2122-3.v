(* Exercice 1 *)
Parameter E : Set.
Parameter Q P : E -> Prop.

Goal (exists x : E, (P x)) /\ (forall y : E, (P y) -> (Q y)) -> (exists w : E, (Q w)).
intro.
elim H.
intros.
elim H0.
intros.
exists x.
apply H1.
assumption.
Qed.

Goal (exists x : E, (P x) /\ (Q x)) -> (exists y : E, (P y) -> (Q y)).
intros.
elim H.
intros.
exists x.
intro.
elim H0.
intros.
assumption.
Qed.

(* Exercice 2 *)
Parameter R : Set.
Parameters zero one : R.
Parameter opp : R -> R.
Parameters plus mult : R -> R -> R.

Section Commutative_ring.

Variables a b c : R.

Axiom ass_plus : plus (plus a b) c = plus a (plus b c).
Axiom com_plus : plus a b = plus b a.
Axiom com_mult : mult a b = mult b a.
Axiom ass_mult : mult (mult a b) c = mult a (mult b c).
Axiom dis_left : mult a (plus b c) = plus (mult a b) (mult a c).
Axiom dis_ right : mult (plus b c) a = plus (mult b a) (mult c a).
Axiom neu_plus_r : plus a zero = a.
Axiom neu_plus_l : plus zero a = a.
Axiom neu_mult_r : mult a one = a.
Axiom neu_mult_l : mult one a = a.
Axiom opp_right : plus a (opp a) = zero.
Axiom opp_left : plus (opp a) a = zero.
End Commutative_ring.

Goal forall a b, mult (plus a b) (plus one one) = plus b (plus a (plus b a)).
intros.
rewrite dis_left.
rewrite neu_mult_r.
rewrite <- ass_plus.
rewrite com_plus.
rewrite (com_plus (plus a b)).
rewrite (com_plus a b).
reflexivity.
Qed.


(* Exercice 3 *)
Require Export List.
Open Scope list_scope.
Import List Notations.

Inductive is_length : list nat -> nat -> Prop :=
| is_length_nil : is_length nil 0
| is_length_cons : forall (n e : nat) (l : list nat) ,
  is_length l n -> is_length (e::l) (S n).

Fixpoint length (l : list nat) {struct l} : nat :=
match l with
| nil => 0
| e::q => S (length q)
end.

Require Import FunInd.
Functional Scheme length_ind := Induction for length Sort Prop.

Goal (forall l : list nat, forall n : nat, (length l) = n -> (is_length l n)).
Proof.
intro.
functional induction (length l) using length_ind.
intros.
rewrite <- H.
apply is_length_nil.
intros.
rewrite <- H.
apply is_length_cons.
apply IHn.
reflexivity.
Qed.


