Require Export ZArith.
Require Export Lia.

Local Open Scope Z.

(* syntaxe *)

Inductive expr : Set :=
| Cte : Z -> expr
| Plus : expr -> expr -> expr
| Moins : expr -> expr -> expr
| Mult : expr -> expr -> expr
| Div : expr -> expr -> expr.

(* sémantique *)

Inductive eval : expr -> Z -> Prop :=
| ECte : forall c : Z, eval (Cte c) c
| EPlus : forall (e1 e2 : expr) (v1 v2 v : Z),
    eval e1 v1 -> eval e2 v2 ->
    v = v1 + v2 -> eval (Plus e1 e2) v
| EMoins : forall (e1 e2 : expr) (v1 v2 v : Z),
    eval e1 v1 -> eval e2 v2 ->
    v = v1 - v2 -> eval (Moins e1 e2) v
| EMult : forall (e1 e2 : expr) (v1 v2 v : Z),
    eval e1 v1 -> eval e2 v2 ->
    v = v1 * v2 -> eval (Mult e1 e2) v
| EDiv : forall (e1 e2 : expr) (v1 v2 v : Z),
    eval e1 v1 -> eval e2 v2 ->
    v = v1 / v2 -> eval (Div e1 e2) v.

(* interprète *)

Fixpoint f_eval (e : expr) : Z :=
    match e with
    | Cte c => c
    | Plus e1 e2 =>
        let v1 := f_eval e1 in
        let v2 := f_eval e2 in
        v1 + v2
    | Moins e1 e2 =>
        let v1 := f_eval e1 in
        let v2 := f_eval e2 in
        v1 - v2
    | Mult e1 e2 =>
        let v1 := f_eval e1 in
        let v2 := f_eval e2 in
        v1 * v2
    | Div e1 e2 =>
        let v1 := f_eval e1 in
        let v2 := f_eval e2 in
        v1 / v2
    end.

(* test *)

Goal eval (Plus (Cte 1) (Cte 1)) 2.
Proof.
    apply (EPlus _ _ 1 1 _).
    - apply ECte.
    - apply ECte.
    - simpl; reflexivity.
Qed.

(* correction feval(e) = v alors e ---> v *)
(* induction sur e *)

Require Import FunInd.
Functional Scheme f_eval_ind := Induction for f_eval Sort Prop.

Lemma correc_eval: forall e:expr, forall v: Z, f_eval(e) = v -> eval e v.
Proof.
  intro.
  functional induction (f_eval e) using f_eval_ind.
  - intros.
    rewrite <- H.
    apply ECte.
  - intros.
    apply (EPlus e1 e2 (f_eval e1) (f_eval e2) v).
    + apply IHz.
      reflexivity.
    + apply IHz0.
      reflexivity.
    + rewrite <- H.
      reflexivity.
  - intros.
    apply (EMoins e1 e2 (f_eval e1) (f_eval e2) v).
    + apply IHz.
      reflexivity.
    + apply IHz0.
      reflexivity.
    + rewrite <- H.
      reflexivity.
  - intros.
    apply (EMult e1 e2 (f_eval e1) (f_eval e2) v).
    + apply IHz.
      reflexivity.
    + apply IHz0.
      reflexivity.
    + rewrite <- H.
      reflexivity.
  - intros.
    apply (EDiv e1 e2 (f_eval e1) (f_eval e2) v).
    + apply IHz.
      reflexivity.
    + apply IHz0.
      reflexivity.
    + rewrite <- H.
      reflexivity.
Qed.

(* complétude e ---> v alors feval(e) = v *)
(* induction sur la relation (elim e--->v) *)

Lemma compl_eval: forall e:expr, forall v:Z, eval e v -> f_eval(e) = v.
Proof.
  intros.
  elim H.
  - intros.
    simpl.
    reflexivity.
  - intros.
    simpl.
    rewrite H1.
    rewrite H3.
    rewrite H4.
    reflexivity.
  - intros.
    simpl.
    rewrite H1.
    rewrite H3.
    rewrite H4.
    reflexivity.
  - intros.
    simpl.
    rewrite H1.
    rewrite H3.
    rewrite H4.
    reflexivity.
- intros.
    simpl.
    rewrite H1.
    rewrite H3.
    rewrite H4.
    reflexivity.
Qed.