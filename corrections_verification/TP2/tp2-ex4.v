Open Scope type_scope.

Section Peano.

Parameter N : Set.
Parameter o : N.
Parameter s : N -> N.
Parameters plus mult : N -> N -> N.
Variables x y : N.
Axiom ax1 : ~((s x) = o).
Axiom ax2 : exists z, ~(x = o) -> (s z) = x.
Axiom ax3 : (s x) = (s y) -> x = y.
Axiom ax4 : (plus x o) = x.
Axiom ax5 : (plus x (s y)) = s (plus x y).
Axiom ax6 : (mult x o) = o.
Axiom ax7 : (mult x (s y)) = (plus (mult x y) x).

End Peano.

Lemma peano_ex1 : 
  (plus (s o) (s (s o))) = (s (s (s o))).
Proof.
  rewrite ax5.
  rewrite ax5.
  rewrite ax4.
  reflexivity.
Qed.

Lemma peano_ex2 : 
  (plus (s (s o)) (s (s o))) = (s (s (s (s o)))).
Proof.
  rewrite ax5.
  rewrite ax5.
  rewrite ax4.
  reflexivity.
Qed.

Lemma peano_ex3 :
  (mult (s (s o)) (s (s o))) = (s (s (s (s o)))).
Proof.
  rewrite ax7.
  rewrite ax7.
  rewrite ax6.
  rewrite ax5.
  rewrite ax5.
  rewrite ax5.
  rewrite ax5.
  rewrite ax4.
  rewrite ax4.
  reflexivity.
Qed.

Lemma ax4_inv : (plus o (s o)) = (s o).
Proof.
  intros.
  rewrite ax5.
  rewrite ax4.
  reflexivity.
Qed.

Ltac simplifie := 
  repeat(intro);
  repeat(rewrite ax4 || rewrite ax5 || rewrite ax6 || rewrite ax7);
  try reflexivity.

Lemma peano_ex1b : 
  (plus (s o) (s (s o))) = (s (s (s o))).
Proof.
  simplifie.
Qed.

Lemma peano_ex2b : 
  (plus (s (s o)) (s (s o))) = (s (s (s (s o)))).
Proof.
  simplifie.
Qed.

Lemma peano_ex3b :
  (mult (s (s o)) (s (s o))) = (s (s (s (s o)))).
Proof.
  simplifie.
Qed.

Hint Rewrite ax4 ax5 ax6 ax7 : cherche0.

Lemma peano_ex1c : 
  (plus (s o) (s (s o))) = (s (s (s o))).
Proof.
  autorewrite with cherche0 using try reflexivity.
Qed.

Lemma peano_ex2c : 
  (plus (s (s o)) (s (s o))) = (s (s (s (s o)))).
Proof.
  autorewrite with cherche0 using try reflexivity.
Qed.

Lemma peano_ex3c :
  (mult (s (s o)) (s (s o))) = (s (s (s (s o)))).
Proof.
  autorewrite with cherche0 using try reflexivity.
Qed.