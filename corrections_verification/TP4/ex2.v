(* q1 *)
Inductive is_even : nat -> Prop :=
  | is_even_O : is_even O
  | is_even_S : forall n: nat, is_even n -> is_even(S (S n)).
(* q2 *)
Fixpoint even(n :nat) : bool :=
  match n with
  | O => true
  | 1 => false
  | (S (S n)) => even n
  end.
Require Import FunInd.
Functional Scheme even_ind := Induction for even Sort Prop.
(* q3 *)
Lemma ex2_3 : forall n : nat, even(n) = true -> is_even(n).
Proof.
  intros.
  functional induction (even n) using even_ind.
  apply is_even_O.
  inversion H.
  apply is_even_S.
  apply IHb.
  apply H.
Qed.
(* q4 *)
Lemma ex2_4 : forall n : nat, even(n) = false -> not( is_even(n) ).
Proof.
  intros.
  induction n.
  intro.
  inversion H.
  induction n.
  intro.
  inversion H0.
  
  intro.
  inversion IHn.
Qed.