(* definition de factorielle *)
(* q1 *)
Inductive is_fact : nat -> nat -> Prop :=
  | is_fact_O : is_fact O (S O)
  | is_fact_S : forall n s : nat,
                is_fact n s -> is_fact (S n) (mult s (S n)).
(* q2 *)
Fixpoint fact (n: nat) : nat :=
  match n with
  | O => (S O)
  | (S n) => (mult (fact n) (S n))
  end.
(* q3 *)
Require Import FunInd.
Functional Scheme fact_ind := Induction for fact Sort Prop.
(* q4 *)
Lemma fact_sound : 
      forall (n s : nat), (fact n) = s -> is_fact n s.
Proof.
  induction n. 
  intros.
  rewrite <- H.
  simpl.
  apply is_fact_O.
  intros.
  rewrite <- H.
  simpl.
  apply is_fact_S.
  apply IHn.
  reflexivity.
Qed.
(* q5 *)
Lemma fact_sound2 : 
      forall (n s : nat), (fact n) = s -> is_fact n s.
Proof.
  intro.
  functional induction (fact n) using fact_ind.
  intros.
  rewrite <- H.
  apply is_fact_O.
  intros.
  rewrite <- H.
  apply is_fact_S.
  (* trivial *)
  apply IHn0.
  reflexivity.
Qed.
(* q6 *)
Lemma fact_complete :
      forall (n s : nat), is_fact n s -> (fact n) = s.
Proof.
  intros.
  elim H.
  intros.
  simpl.
  reflexivity.
  intros.
  simpl.
  rewrite H1.
  reflexivity.
Qed.
