Open Scope list.

Parameter E: Type.

Fixpoint rev (l: list E) {struct l} : list E :=
  match l with
    | nil => nil
    | e::q => (rev q) ++ (e::nil)
  end.

Lemma ex2_2: 
  forall l: list E, forall e: E, 
  rev(l ++ (e::nil)) = e::rev(l).
Proof.
  intros.
  elim l.
  simpl.
  reflexivity.
  intros.
  simpl.
  rewrite H.
  reflexivity.
Qed.

Lemma ex2_3: forall l: list E, rev(rev(l)) = l.
Proof.
  intros.
  elim l.
  simpl.
  reflexivity.
  intros.
  simpl.
  rewrite ex2_2.
  rewrite H.
  reflexivity.
Qed.